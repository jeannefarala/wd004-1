// console.log("hello");
// alert('hello world');

// Variables:
//Data Storage

// To declare a variable, we'll use the keywords let, or const:
// We will create the variable for the first time.

let student = "Brandon";
// console.log(student);

const day = "Monday";
// console.log(day);

// const is short for constant.
// We cannot reassign a value.
// day = "Tuesday";
// console.log(day);
// 
student = "Brenda";
// console.log(student);

student = 3.1416;
// console.log(student);

// Data Types:
// 1. String 
// - denoted by quotation marks
// - series of characters (ex. "Brandon", "1234", "String", "mixed123")
// 2. Number
// - can be used in mathematical operations (ex. 2, 3.15, 0.25, 450)
// 3. Boolean
// - true or false

// To check the data type
console.log(typeof "Am I a string?");
console.log(typeof "1234");
console.log(typeof 1234);
console.log(typeof "true");
console.log(typeof false);
// console.log(typeof whatami);

// Operators:
// A. Arithmatic Operators
// 1. Addition +
// 2. Subtraction -
// 3. Multiplication *
// 4. Division /
const num1 = 10;
const num2 = 2;
const num3 = 6;

console.log(num1 + num2); //ans:12

// Modulo (%) - remainder

// Assignment Operator:
// = - we are assigning a value to a variable
// 
// Assignment-Arithmetic:
// +=, -=, *=, /=, %=

let output = 0;
const num4 = 5;
const num5 = 8;

// output = output + num4;
output += num4; // adds num4 to the previous value of output
// -= subtracts num4 from the previous value


