/**
 * Give at least three reasons why you should learn javascript.
 */

1. JS makes the website more interactive
2. So that you wont have to spend hours coding with pure html and css
3. JS makes the website more user-friendly

/**
 * In two sentences, state in your own words, what is your understanding about Javascript?
 */

1. 
2.

/**
 * What is a variable? Give at least three examples.
 */

1. Variables are empty containers wherein a user may assign any value to it.
Examples:
	1. let x = "forgotten"
	2. const y = 16
	3. jeanne = "cute"

/**
 * What are the data types we talked about today? Give at least two samples each.
 */

===> 1. String 2. Number

/**
 * Write the correct data type of the following:
 */

1. "Number" - string
2. 158 - number
3. pi - number
4. false - boolean
5. 'true' - string
6. 'Not a String' - string

/**
 * What are the arithmetic operators?
 */
 	The Arithmetic Operators are 
 	1. Addition +
	2. Subtraction -
	3. Multiplication *
	4. Division /
/**
 * What is a modulo? 
 */
 	Modulo is the remainder returned after dividing two numbers

/**
 * Interpret the following code
 */

let number = 4;
number += 4;

Q: What is the value of number?
8
----

let number = 4;
number -= 4;

Q: What is the value of number?
0
----

let number = 4;
number *= 4;

Q: What is the value of number?
16
----

let number = 4;
number /= 4;

Q: What is the value of number?
1
----

